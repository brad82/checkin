<?php

class WP_Checkin_Admin
{
  public function __construct()
  {
    add_action('admin_menu', array($this, 'register_options_page'));
    add_action('init', array($this, 'process_options_page'));
  }

  public function register_options_page()
  {
    add_options_page('Check In Settings', 'Check In', 'manage_options', 'checkin', array($this, 'render_options_page'));
  }

  public function process_options_page()
  {
    if (!array_key_exists('wp_checkin_mappings', $_POST) || !check_admin_referer('wp_checkin')) {
      return;
    }

    $matrix = array();
    $mappings = $_POST['wp_checkin_mappings'];
    $statuses = wc_get_order_statuses();

    foreach ($statuses as $id => $label)
      $matrix[$id] = array_key_exists($id, $mappings) ? $mappings[$id] : -1;

    update_option('wp_checkin_matrix', $matrix);

    add_action('admin_notices', array($this, 'admin_notice__success'));
  }

  public function admin_notice__success()
  {
?>
<div class="notice notice-success">
  <p><?= __('Done!', 'wp-checkin') ?></p>
</div>
<?php
  }
  public function render_options_page()
  {
    $statuses = array();
    $matrix = get_option('wp_checkin_matrix');

    foreach (wc_get_order_statuses() as $id => $label) {
      $statuses[$id] = array(
        'label' => $label,
        'mapping' => array_key_exists($id, $matrix) ? $matrix[$id] : -1
      );
    }
?>
<div class="wrap">
  <?php screen_icon(); ?>
  <form method="post">
    <h1>Check In Mappings</h1>
    <button type="submit" class="button button-primary" style="float: right; margin: 20px 0;">Save</button>
    <table class="widefat fixed">
      <tbody>
        <?php $i = 0; foreach($statuses as $id => $status): $i++ ?>
        <tr<?php $i % 2 && print(' class="alternate"') ?>>
          <th scope="row" class="manage-column"><?= $status['label'] ?></th>
          <td>
            <select name="wp_checkin_mappings[<?= $id ?>]">
              <option value="-1">[No Action]</option>
              <?php foreach(wc_get_order_statuses() as $_id => $_label): ?>
              <option value="<?= $_id ?>"<?php $status['mapping'] == $_id && print(' selected') ?>>
                <?= $_label ?>
              </option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <button type="submit" class="button button-primary" style="float: right; margin: 20px 0;">Save</button>
    <?php wp_nonce_field('wp_checkin'); ?>
  <form>
</div>
<?php
  }
}

new WP_Checkin_Admin();