<?php
/*
Plugin Name: WP Checkin
Description: Connector plugin for the Coexiste Check In Kiosks
Version: 1.0.0
Author: Brad Morris (Codeable)
Author URI: http://bradleymorris.co.uk
*/
class WP_Checkin
{
  const NAMESPACE = 'wp-checkin/v1';

  public function __construct()
  {

    require_once 'wp-checkin-admin.php';

    add_action('rest_api_init', array($this, 'register_routes'));
  }

  public function register_routes()
  {
    register_rest_route(static::NAMESPACE, 'roster', array(
      'methods' => 'GET',
      'callback' => array($this, 'get_roster')
    ));

    register_rest_route(static::NAMESPACE, 'roster', array(
      'methods' => 'POST',
      'callback' => array($this, 'post_roster')
    ));

    register_rest_route(static::NAMESPACE, 'statuses', array(
      'methods' => 'GET',
      'callback' => array($this, 'get_statuses')
    ));
  }

  protected function get_order_ids_by_product_id($product_id, $order_statuses = array('wc-completed'))
  {
    global $wpdb;

    $statuses = array();
    foreach($order_statuses as $status) {
      $statuses[] = esc_sql($status);
    }

    $query = $wpdb->prepare("
      SELECT order_items.order_id as order_id, post_meta.meta_value as customer_id, posts.post_status as post_status
      FROM {$wpdb->prefix}woocommerce_order_items as order_items
      LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
      LEFT JOIN {$wpdb->posts} AS posts
        ON order_items.order_id = posts.ID
      LEFT JOIN {$wpdb->postmeta} AS post_meta
        ON post_meta.post_id = order_id
        AND post_meta.meta_key = '_customer_user'
      WHERE posts.post_type = 'shop_order'
      AND posts.post_status IN ('".join("','", $statuses)."')
      AND order_items.order_item_type = 'line_item'
      AND order_item_meta.meta_key = '_product_id'
      AND order_item_meta.meta_value = '%d'
    ", array($product_id));

    $results = $wpdb->get_results($query);

    return $results;
  }

  /**
   * Expects a list of products, will return users that have orders
   */
  public function get_roster(WP_REST_Request $request)
  {
    $product_id = $request->get_param('product_id');
    $statuses = $request->get_param('order_status');
    $statuses = explode(',', $statuses);

    $orders = $this->get_order_ids_by_product_id($product_id, $statuses);

    $roster = array();

    foreach($orders as $order)
    {
      $roster[] = array(
        'order_id'      => $order->order_id,
        'order_status'  => $order->post_status,
        'product_id'    => $product_id,
        'user_id'       => $order->customer_id,
      );
    }

    return $roster;
  }

  /**
   * Expects a list of products, will return users that have orders
   */
  public function post_roster(WP_REST_Request $request)
  {
    $matrix = get_option('wp_checkin_matrix');

    $payload = $request->get_json_params();
    $result = array();

    foreach($payload as $checkin)
    {
      if (!$order = new WC_Order($checkin['orderId']))
        continue;

      $currentStatus = 'wc-'.$order->get_status();
      $updatedStatus = $matrix[$currentStatus];

      if(!$updatedStatus || $updatedStatus == -1)
        continue;

      $order->update_status($updatedStatus, 'Via Check In Kiosk');

      $result[$checkin['orderId']] = $updatedStatus;
    }

    return $result;
  }

  public function get_statuses(WP_REST_Request $request)
  {
    return wc_get_order_statuses();
  }
}

new WP_Checkin;