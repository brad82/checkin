import { camelCase } from 'lodash'
const createAsyncMutation = (type) => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  COMPLETE: `${type}_COMPLETE`,
  CHUNKS: `${type}_CHUNKS`,
  TOTAL_PAGES: `${type}_TOTAL_PAGES`,
  NEXT_PAGE: `${type}_NEXT_PAGE`,
  loadingKey: camelCase(`${type}_PENDING`),
  stateKey: camelCase(`${type}_DATA`)
})

export default {
  createAsyncMutation
}
