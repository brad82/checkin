import axios from 'axios'

function _url ({ host }, endpoint) {
  return host + '/Bayometric/BFSAPI/json/' + endpoint
}

export default {
  state: {
    host: '',
    status: false,
    sessionKey: ''
  },
  mutations: {
    SET_HOST (state, host) {
      state.host = host
    },
    SET_SESSION_KEY (state, sessionKey) {
      state.sessionKey = sessionKey
    }
  },
  actions: {
    setFingerprintHost ({ commit }, host) {
      commit('SET_HOST', host)
    },
    createSession ({ commit, state }) {
      return axios.post(_url(state, 'CreateSession'), {
        authRequestInfo: {
          'PersonID': null,
          'Password': null,
          'ShowGUI': true,
          'Timeout': 60
        }
      })
        .then((response) => {
          if (response.data.ResponseCode === 0) {
            commit('SET_SESSION_KEY', response.data.SessionKey)
          } else {
            alert('Could not start fingerprint session')
          }
        })
        .catch((error) => {
          alert('Could not start fingerprint session: (' + error + ')')
        })
    },
    enrollUser ({ commit, state }, userId) {
      return new Promise((resolve, reject) => {
        axios.post(_url(state, 'RegisterPerson'), {
          sessionKey: state.sessionKey,
          personID: userId
        }).then((response) => {
          if (response.data.ResponseCode === 0) {
            commit('USER_ENROLLED', 1)
            resolve(userId)
          } else {
            reject(response.data.ResponseCode, response.data.ReturnMessage, response.data)
          }
        })
          .catch(reject)
      })
    },
    requestScan ({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios.post(_url(state, 'Search'), {
          timeout: 15000
        })
          .then((response) => {
            if (response.data.ResponseCode === 2) {
              resolve(response.data.PersonFoundID)
            } else {
              reject(response.dataResponseCode, response)
            }
          })
          .catch(reject)
      })
    }
  },
  getters: {
    fingerprintServerConnectionStatus: (state) => {
      return state.sessionKey.length > 0
    }
  }
}
