import { shuffle } from 'lodash'
import asyncUtils from '@/store/utils/asyncUtils.js'

const GET_ROSTERS_ASYNC = asyncUtils.createAsyncMutation('GET_ROSTERS')

export default {
  state: {
    roster: [],
    loaded: [],
    pending: []
  },
  mutations: {
    [GET_ROSTERS_ASYNC.PENDING] (state, productId) {
      state.pending.push(productId)
    },
    [GET_ROSTERS_ASYNC.SUCCESS] (state, { productId, roster }) {
      state.pending.splice(state.pending.indexOf(productId), 1)
      state.loaded.push(productId)
      roster.map(item => {
        state.roster.push({
          orderId: parseInt(item.order_id),
          productId: parseInt(item.product_id),
          userId: parseInt(item.user_id),
          checkedIn: false,
          pending: false,
          timestamp: null
        })
      })
    },
    [GET_ROSTERS_ASYNC.FAILURE] (state, { productId, error }) {
      state.pending.splice(state.pending.indexOf(productId), 1)
    },
    [GET_ROSTERS_ASYNC.COMPLETE] (state, productId) {
      state.pending.splice(state.pending.indexOf(productId), 1)
    },
    CHECK_IN (state, index) {
      state.roster[index].checkedIn = true
      state.roster[index].pending = true
      state.roster[index].timestamp = Date.now()
    },
    CHECK_OUT (state, index) {
      state.roster[index].checkedIn = false
      state.roster[index].pending = false
    },
    CLEAR_PRODUCT_FROM_ROSTER (state, productId) {
      for (let i = state.roster.length - 1; i >= 0; i--) {
        if (state.roster[i].productId === productId) {
          state.roster.splice(i, 1)
        }
      }
      state.loaded.splice(state.loaded.indexOf(productId), 1)
    },
    ENTRIES_COMMITTED (state, orderIds) {
      orderIds = orderIds.map(id => parseInt(id))
      for (let i = state.roster.length - 1; i >= 0; i--) {
        let index = orderIds.indexOf(state.roster[i].orderId)
        if (index > -1) {
          state.roster[i].pending = false
        }
      }
    }
  },
  actions: {
    checkIn ({ commit, state }, { productId, userId }) {
      return new Promise((resolve, reject) => {
        state.roster.forEach((item, index) => {
          if (item.productId === parseInt(productId) && item.userId === parseInt(userId)) {
            if (item.checkedIn === false) {
              commit('CHECK_IN', index)
              return resolve(item)
            }
          }
        })

        reject(new Error('No valid class found'))
      })
    },
    checkOut ({ commit, state }, { productId, userId }) {
      state.roster.forEach((item, index) => {
        if (item.productId === parseInt(productId) && item.userId === parseInt(userId)) {
          commit('CHECK_OUT', index)
        }
      })
    },
    loadRoster ({ commit, dispatch }, { productId, statuses }) {
      commit(GET_ROSTERS_ASYNC.PENDING, productId)
      return dispatch('wpApi_getSimple', {
        endpoint: 'wp-checkin/v1/roster',
        data: {
          'product_id': productId,
          'order_status': statuses.join(',')
        }
      })
        .then(response => {
          let payload = {
            productId: productId,
            roster: response.data
          }
          commit(GET_ROSTERS_ASYNC.SUCCESS, payload)
          return { ...payload, response }
        })
        .catch(error => {
          commit(GET_ROSTERS_ASYNC.FAILURE, {
            productId: productId,
            error: error
          })
        })
    },
    commitRoster ({ commit, dispatch, state }, productId) {
      commit(GET_ROSTERS_ASYNC.PENDING, productId)
      let payload = state.roster.filter(item => {
        return item.productId === parseInt(productId) &&
          item.checkedIn === true &&
          item.pending === true
      })
      console.log(payload)
      return dispatch('wpApi_postSimple', {
        endpoint: 'wp-checkin/v1/roster',
        data: payload
      })
        .then(response => {
          commit(GET_ROSTERS_ASYNC.COMPLETE, productId)
          commit('ENTRIES_COMMITTED', Object.keys(response.data))
        })
    },
    commitAllRosters ({ commit, dispatch, state }) {
      state.loaded.map((productId) => {
        dispatch('commitRoster', productId)
      })
    },
    closeRoster ({ commit }, productId) {
      commit('CLEAR_PRODUCT_FROM_ROSTER', parseInt(productId))
    }
  },
  getters: {
    getRoster: state => state.roster,
    loadedRosters: state => state.loaded,
    pendingRosters: state => state.pending,
    pendingSubmissions: state => state.roster.filter(item => item.pending),
    mockUsers (state) {
      let mock = {single: [], multiple: []}
      let count = {}
      let ids = shuffle(state.roster.map(item => item.userId))

      ids.forEach(i => {
        count[i] = (count[i] || 0) + 1
      })

      for (var id in count) {
        mock[(count[id] === 1 ? 'single' : 'multiple')].push(id)
      }

      return mock
    }
  }
}
