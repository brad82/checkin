import _ from 'lodash'
import asyncUtils from '@/store/utils/asyncUtils.js'

const GET_USERS_ASYNC = asyncUtils.createAsyncMutation('GET_USERS')

export default {
  state: {
    users: [],
    loading: 0
  },
  mutations: {
    [GET_USERS_ASYNC.PENDING] (state) {
      state.loading++
    },
    [GET_USERS_ASYNC.CHUNKS] (state, chunks) {
      state.chunks = chunks
    },
    [GET_USERS_ASYNC.SUCCESS] (state, users) {
      state.loading--
      users.map((user) => {
        state.users.push({
          id: user.id,
          avatar: user.avatar_urls[48],
          fullName: user.first_name + ' ' + user.last_name,
          firstName: user.first_name,
          lastName: user.last_name,
          email: user.email
        })
      })
    },
    [GET_USERS_ASYNC.FAILURE] (state, error) {
      state.loading--
    },
    CLEAR_USERS (state) {
      state.users = []
    }
  },
  actions: {
    loadUsers ({ commit, dispatch }, page = 1) {
      commit('CLEAR_USERS')
      return dispatch('wpApi_getChunked', {
        endpoint: 'wp/v2/users?context=edit',
        mutators: GET_USERS_ASYNC
      })
    }
  },
  getters: {
    getRandomUserId: (state) => {
      return (state.users.length > 0 ? _.sample(state.users).id : false)
    },
    getUsers: (state) => {
      return state.users
    },
    getUsersGroupedByLastName: (state) => {
      return state.users.length > 0 ? _(state.users).sortBy((user) => user.lastName)
        .groupBy((item) => {
          if (!item.lastName) {
            return '#'
          }
          return item.lastName.substr(0, 1).toUpperCase()
        })
        .value() : []
    },
    loadingUsers: state => {
      if (state.chunks) {
        return {
          total: state.chunks,
          complete: state.chunks - state.loading,
          pending: state.loading,
          isLoading: state.loading > 0
        }
      } else {
        return {
          total: -1,
          complete: -1,
          pending: state.loading,
          isLoading: state.loading > 0
        }
      }
    }
  }
}
