import axios from 'axios'
import router from '../../router/index.js'

export default {
  state: {
    domain: '',
    token: '',
    username: '',
    loading: false
  },
  mutations: {
    WPAPI_PENDING (state) {
      state.loading = true
    },
    WPAPI_COMPLETE (state) {
      state.loading = false
    },
    SET_WPAPI_DOMAIN (state, domain) {
      state.domain = domain.replace(/\/$/, '')
    },
    SET_WPAPI_TOKEN (state, token) {
      state.token = token
    },
    SET_WPAPI_USERNAME (state, username) {
      state.username = username
    }
  },
  actions: {
    attemptWpApiLogin (store, { username, password }) {
      store.commit('WPAPI_PENDING')
      return axios.post(store.getters.wpApiDomain + '/wp-json/jwt-auth/v1/token', {
        username, password
      })
        .then(response => {
          store.commit('WPAPI_COMPLETE')
          store.commit('SET_WPAPI_USERNAME', username)
          store.commit('SET_WPAPI_TOKEN', response.data.token)
          return response
        })
        .catch(error => {
          console.error(error)
          store.commit('WPAPI_COMPLETE')
          return error
        })
    },

    wpApi_get ({ commit }, { endpoint, mutators, data }) {
      commit(mutators.PENDING)
      return axios(this.getters.wpApiDomain + '/wp-json/' + endpoint, {
        params: data,
        headers: {
          Authorization: this.getters.wpApiTokenHeader
        }
      })
        .then(response => {
          commit(mutators.SUCCESS, response.data)
          return response
        })
        .catch(error => {
          if (error.response) {
            let data = error.response.data
            switch (error.response.status) {
              case 403:
                console.log(this)
                router.push('/settings/wp-login')
                break
              default:
                alert(`Error: [${data.code}] ${data.message}`)
            }
          }
          commit(mutators.FAILURE, error)
        })
    },

    wpApi_getChunked ({ commit, dispatch }, opts) {
      let pagedOpts = Object.assign({
        data: { 'per_page': 100 }
      }, opts)

      commit(opts.mutators.CHUNKS, -1)

      return dispatch('wpApi_get', pagedOpts)
        .then(response => {
          let pages = parseInt(response.headers['x-wp-totalpages'])

          commit(opts.mutators.CHUNKS, pages - 1)

          for (let i = 1; i < pages; i++) {
            dispatch('wpApi_get', Object.assign({
              data: { 'per_page': 100, page: i + 1 }
            }, opts))
          }
        })
    },

    wpApi_getSimple (store, { endpoint, data }) {
      return axios(this.getters.wpApiDomain + '/wp-json/' + endpoint, {
        params: data,
        headers: {
          Authorization: this.getters.wpApiTokenHeader
        }
      })
    },

    wpApi_postSimple (store, { endpoint, data }) {
      return axios.post(this.getters.wpApiDomain + '/wp-json/' + endpoint, data, {
        headers: {
          Authorization: this.getters.wpApiTokenHeader
        }
      })
    }
  },
  getters: {
    wpApiDomain: state => state.domain,
    wpApiLoading: state => state.loading,
    wpApiUsername: state => state.username,
    wpApiIsReady: state => {
      return state.token.length > 0 && state.domain.length > 0
    },
    wpApiTokenHeader: state => {
      return 'Bearer ' + state.token
    }
  }
}
