import _ from 'lodash'
import asyncUtils from '@/store/utils/asyncUtils.js'

const GET_CLASSES_ASYNC = asyncUtils.createAsyncMutation('GET_CLASSES')

export default {
  state: {
    classes: [],
    loading: 0
  },
  mutations: {
    [GET_CLASSES_ASYNC.PENDING] (state) {
      state.loading++
    },
    [GET_CLASSES_ASYNC.SUCCESS] (state, classes) {
      state.loading--
      classes.map((_class) => {
        state.classes.push({
          id: _class.id,
          name: _class.name
        })
      })

      state.classes = _.orderBy(state.classes, 'name')
    },
    [GET_CLASSES_ASYNC.FAILURE] (state, error) {
      state.loading--
    },
    CLEAR_CLASSES (state) {
      state.classes = [];
    }
  },
  actions: {
    loadClasses ({ commit, dispatch }) {
      commit('CLEAR_CLASSES')
      dispatch('wpApi_getChunked', {
        endpoint: 'wc/v2/products?per_page=100',
        mutators: GET_CLASSES_ASYNC
      })
    }
  },
  getters: {
    getClasses: (state) => state.classes,
    getRandomClassId: (state) => {
      return state.classes.length > 0 ? _.sample(state.classes).id : false
    },
    loadingClasses: state => state.loading
  }
}
