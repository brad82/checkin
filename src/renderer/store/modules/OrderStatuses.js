import asyncUtils from '@/store/utils/asyncUtils.js'

const GET_ORDER_STATUSES_ASYNC = asyncUtils.createAsyncMutation('GET_ORDER_STATUSES')

export default {
  state: {
    statuses: [],
    loading: false
  },
  mutations: {
    [GET_ORDER_STATUSES_ASYNC.PENDING] (state) {
      state.loading = true
    },
    [GET_ORDER_STATUSES_ASYNC.SUCCESS] (state, statuses) {
      state.loading = false
      state.statuses = []
      for (let key in statuses) {
        state.statuses.push({
          id: key,
          label: statuses[key]
        })
      }
    },
    [GET_ORDER_STATUSES_ASYNC.FAILURE] (state, error) {
      state.loading = false
    }
  },
  actions: {
    loadStatuses ({ commit, dispatch }) {
      return dispatch('wpApi_get', {
        endpoint: 'wp-checkin/v1/statuses',
        mutators: GET_ORDER_STATUSES_ASYNC
      })
    }
  },
  getters: {
    getStatuses: state => state.statuses,
    loadingStatuses: state => state.loading
  }
}
