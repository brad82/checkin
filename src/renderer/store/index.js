import createPersistedState from 'vuex-persistedstate'
import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules'

const persistedState = createPersistedState({
  paths: [
    'Classes.classes',
    'Fingerprint.host',
    'OrderStatuses.statuses',
    'Roster.roster',
    'Roster.loaded',
    'Users.users',
    'WpApi'
  ]
})

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  plugins: [ persistedState ],
  strict: process.env.NODE_ENV !== 'production'
})
