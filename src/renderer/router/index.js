import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: require('@/components/CheckIn').default,
      children: [
        {
          path: '',
          name: 'front',
          component: require('@/components/CheckIn/Begin').default
        },
        {
          path: 'login-with-id',
          component: require('@/components/CheckIn/LoginWithId').default
        },
        {
          path: 'scan',
          component: require('@/components/CheckIn/Scan').default
        },
        {
          path: 'check-in/:userId',
          props: true,
          component: require('@/components/CheckIn/AttemptCheckIn').default
        },
        {
          path: 'complete/:userId/:productId',
          props: true,
          component: require('@/components/CheckIn/Complete').default
        }
      ]
    },
    {
      path: '/error',
      name: 'error',
      component: require('@/components/Error').default
    },
    {
      path: '/settings',
      component: require('@/components/Settings').default,
      meta: {
        breadcrumb: 'Settings'
      },
      children: [
        {
          path: 'load-classes',
          name: 'load-classes',
          component: require('@/components/Settings/LoadClasses').default,
          meta: {
            requiresWpConnection: true
          }
        },
        {
          path: 'manual-check-in',
          name: 'checkin',
          component: require('@/components/Settings/ManualCheckIn').default
        },
        {
          path: 'enroll-user',
          name: 'enroll-user',
          component: require('@/components/Settings/EnrollUser').default,
          meta: {
            requiresWpConnection: true
          }
        },
        {
          path: 'fingerprint-server',
          name: 'fingerprint-server',
          component: require('@/components/Settings/FingerprintServer').default
        },
        {
          path: 'updates',
          name: 'updates',
          component: require('@/components/Settings/Updates').default
        },
        {
          path: 'wp-login',
          name: 'wp-login',
          component: require('@/components/Settings/WpLogin').default
        },
        {
          path: 'wp-sync',
          name: 'wp-sync',
          component: require('@/components/Settings/WpSync').default,
          meta: {
            requiresWpConnection: true
          }
        }
      ]
    },
    {
      path: '*',
      component: require('@/components/PageNotFound').default
    }
  ]
})
