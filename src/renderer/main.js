import { remote } from 'electron'
import Vue from 'vue'
import VueBreadcrumbs from 'vue-breadcrumbs'
import VueI18n from 'vue-i18n'
import VueRollbar from 'vue-rollbar'

import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'

import { faExclamationTriangle, faUserPlus, faSync, faFingerprint, faSignOutAlt, faSpinner, faHandPointUp, faThumbsUp, faHome, faCog, faKey } from '@fortawesome/free-solid-svg-icons'
import { faWordpressSimple } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add({ faExclamationTriangle, faUserPlus, faSync, faFingerprint, faSignOutAlt, faSpinner, faHandPointUp, faThumbsUp, faHome, faCog, faWordpressSimple, faKey })

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))

window.appConfig = remote.getGlobal('appConfig')

Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.config.devtools = true

Vue.use(BootstrapVue)
Vue.use(VueBreadcrumbs)
Vue.use(VueI18n)
Vue.use(VueRollbar, {
  accessToken: 'c431472bbb434abeb431766fa47cffb7',
  enabled: true,
  payload: {
    environment: process.env.NODE_ENV
  }
})

Vue.component('fa-icon', FontAwesomeIcon)

const i18n = new VueI18n({
  locale: window.appConfig.locale,
  fallbackLocale: 'en-gb',
  // silentTranslationWarn: true,
  messages: {
    'en-gb': require('../locales/en-gb.json'),
    'pt-br': require('../locales/pt-br.json'),
    'es-br': require('../locales/es-br.json')
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresWpConnection) && !store.getters.wpApiIsReady) {
    next({
      path: '/settings/wp-login',
      query: {
        redirect: to.fullPath
      }
    })
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  components: { App },
  i18n,
  router,
  store,
  template: '<App/>'
}).$mount('#app')
