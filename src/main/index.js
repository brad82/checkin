import { app, autoUpdater, BrowserWindow } from 'electron' // eslint-disable-line
import Config from 'electron-config'

let config = new Config({
  defaults: {
    passcode: 1337,
    locale: 'pt-br'
  }
})

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\') // eslint-disable-line
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080'
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */

  global.appConfig = config.store

  mainWindow = new BrowserWindow({
    height: 768,
    useContentSize: true,
    width: 1024,
    frame: process.env.NODE_ENV === 'development'
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  if (process.env.NODE_ENV !== 'development') {
    mainWindow.maximize()
    mainWindow.setFullScreen(true)
  }
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
